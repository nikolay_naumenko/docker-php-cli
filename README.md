# Docker PHP CLI Image

Docker should be installed on your system. 

## Installation

You can pull a pre-built image from Docker hub:

```bash
$ docker pull nikolaynaumenko/php-cli:latest
```

Or build it locally:

```bash
$ docker build -t php-cli .
```

## Usage

Run PHP script:

```bash
$ docker run -it --rm --name php-cli-running -v "${PWD}":/usr/src/app -w /usr/src/app nikolaynaumenko/php-cli php pathToScript.php
```

Run Composer:

```bash
$ docker run -it --rm --name php-cli-running -v "${PWD}":/usr/src/app -w /usr/src/app nikolaynaumenko/php-cli composer dumpautoload
```

Run PHPUnit:

```bash
$ docker run -it --rm --name php-cli-running -v "${PWD}":/usr/src/app -w /usr/src/app nikolaynaumenko/php-cli phpunit
```

These commands should work on Unix systems (MacOS, Linux) and Windows PowerShell.

You can also add short aliases to your .bash_profile or .zshrc file:

```bash
alias php='docker run -it --rm --name php-running-script -v "$PWD":/usr/src/app -w /usr/src/app nikolaynaumenko/php-cli  php'

alias composer='docker run -it --rm --name php-running-script -v "$PWD":/usr/src/app -v ~/php:/composer -w /usr/src/app nikolaynaumenko/php-cli composer'

alias phpunit='docker run -it --rm --name php-running-script -v "$PWD":/usr/src/app -v ~/php:/phpunit -w /usr/src/app nikolaynaumenko/php-cli phpunit'
```
