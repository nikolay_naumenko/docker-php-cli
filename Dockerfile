FROM php:cli
RUN apt-get update \
    && apt-get install apt-utils -y
RUN apt-get install git curl wget zip unzip sqlite3 libsqlite3-dev libpq-dev -y
RUN docker-php-ext-install pdo pdo_sqlite pdo_mysql pdo_pgsql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN wget https://phar.phpunit.de/phpunit.phar \
    && chmod +x phpunit.phar \
    && mv phpunit.phar /usr/local/bin/phpunit